FROM node:11.9-alpine

RUN adduser -D appuser
USER appuser

RUN mkdir -p /home/appuser/app
WORKDIR /home/appuser/app

COPY package.json yarn.lock /home/appuser/app/
# RUN yarn install && yarn cache clean
RUN npm install
COPY . /home/appuser/app/

CMD ["node", "app"]
