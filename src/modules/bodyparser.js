const cobody = require('co-body')

async function parse(ctx, next) {
  try {
    ctx.request.body = await cobody.json(ctx)
  } catch (err) {
    ctx.throw(400, "Malformed json, check your format")
  }
  await next()
}

module.exports = parse
