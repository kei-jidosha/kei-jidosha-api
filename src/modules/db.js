const MongoClient = require('mongodb').MongoClient

let db = {}

module.exports.put = (id, item, collection) => {
  const coll = db.collection(collection)
  return coll.update({ _id: id }, item, { upsert: true })
}

module.exports.get = (id, collection) => {
  const coll = db.collection(collection)
  return coll.findOne({ _id: id })
}

module.exports.list = (collection) => {
  const coll = db.collection(collection)
  return coll.find({}).toArray()
}

module.exports.delete = (id, collection) => {
  const coll = db.collection(collection)
  return coll.deleteOne({ _id: id })
}

module.exports.startup = async (connectionString) => {
  try {
    db = await MongoClient.connect(connectionString)
  } catch (err) {
    throw Object.assign(err, { message: `Error when connection to ${connectionString}`, innerMessage: err.message })
  }
}

module.exports.close = () => {
  db.close()
}
