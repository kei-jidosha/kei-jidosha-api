const validations = {
  "No service id provided": (app) => app.service && app.service.id,
  "No application name provided": (app) => app.application && app.application.name && app.application.name.length > 0,
  "No valid input defined (type && href)": (app) => app.dependsOn && !app.dependsOn.some(x => !x.type || !x.name),
  "No valid output defined (type && href)": (app) => app.output && !app.output.some(x => !x.type || !x.name),
  "No valid instance proivided (version && id)": (app) => app.instance && app.instance.version && app.instance.id,
}

const validateRule = (item, rule) => {
  try {
    return rule(item)
  } catch (err) {
    return false
  }
}

const validate = (item, rules) => {
  const msgs = Object.entries(rules)
    .filter(([, rule]) => !validateRule(item, rule))
    .map(([msg]) => msg)

  return {
    "isValid": msgs.length === 0,
    "msgs": msgs
  }
}

module.exports.validate = (service) => {
  return validate(service, validations)
}
