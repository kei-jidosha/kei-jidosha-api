const router = require("koa-router")()
const bodyparser = require("../modules/bodyparser")
const repository = require("./repository")

router.get("/", async (ctx, next) => {
  ctx.body = await repository.list()

  await next()
})

router.post("/", bodyparser, async (ctx, next) => {
  const response = await repository.createUpdate(ctx.request.body)
  console.log(response)
  if (response.status !== "success") {
    ctx.throw(response.code, response.msgs.join(" | "))
  } else {
    ctx.body = response
  }
  await next()
})

router.get("/:appId", async (ctx, next) => {
  ctx.body = await repository.getById(ctx.params.appId)

  await next()
})

router.del("/:appId", async (ctx, next) => {
  await repository.removeById(ctx.params.appId)
  ctx.status = 204

  await next()
})

router.del("/:appId/instances/:instanceid", async (ctx, next) => {
  await repository.removeInstanceById(ctx.params.appId, ctx.params.instanceid)
  ctx.status = 204

  await next()
})

router.post("/:appId/instances/:instanceid/heartbeat", async (ctx, next) => {
  const response = await repository.updateInstanceHeartbeat(ctx.params.appId, ctx.params.instanceid)

  ctx.body = response
  ctx.status = response.code

  await next()
})

module.exports = router.routes()
