const db = require("../modules/db")
const validator = require("./validator")
const idGenerator = require("./id-generator")

const _collection = "applications"

const createUpdate = async (app) => {
  const validation = validator.validate(app)
  if (!validation.isValid) {
    return {
      "status": "invalid",
      "code":400,
      "msgs": validation.msgs
    }
  }

  const id = idGenerator.generate(app)
  const a = await db.get(id, _collection) || {}

  const date = new Date().toISOString()
  app.instance.startedOn = date
  const instances = a.instances || {}
  instances[app.instance.id] = app.instance
  app.instances = instances
  delete app.instance

  db.put(id, app, _collection)
  console.log("Successfully wrote application", id)

  return {
    "status": "success",
    "appId": id
  }
}

const list = async () => {
  return await db.list(_collection)
}

const getById = async (id) => {
  return await db.get(id, _collection)
}

const removeById = async (id) => {
  await db.delete(id, _collection)
  console.log("Successfully removed application", id)
}

const removeInstanceById = async (applicationId, instanceId) => {
  const service = await db.get(applicationId, _collection)

  delete service.instances[instanceId]

  await db.put(applicationId, service, _collection)
  console.log("Successfully removed instance", instanceId)

  return service
}

const updateInstanceHeartbeat = async (appId, instanceId) => {
  const app = await db.get(appId, _collection)
  if (!app || !app.instances[instanceId]) {
    return {
      status: "error",
      code: 404,
      msg: "No application or instance found with given id:s",
    }
  }

  const now = new Date()
  app.instances[instanceId].heartbeat = now.toISOString()
  db.put(appId, app, _collection)

  return {
    status: "success",
    code: 200,
    msg: "Pulsating..."
  }
}

module.exports = {
  getById,
  list,
  createUpdate,
  removeById,
  removeInstanceById,
  updateInstanceHeartbeat,
}
