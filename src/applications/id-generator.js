const slugify = require("slugify")

module.exports.generate = (item) => {
  const slug = slugify(item.application.name)
  return `${item.service.id}:${slug}`
}
