const router = require("koa-router")()

router.get("/", async (ctx, next) => {
  const {name: name, description: description} = require("../../package.json")
  ctx.body = {name, description}
  await next()
})

router.get("health", async (ctx, next) => {
  ctx.body = { "status": "ok" }

  await next()
})

router.get("version", async (ctx, next) => {
  const {version} = require("../../package.json")
  ctx.body = { "version": version }

  await next()
})

router.get("monitor", async (ctx, next) => {
  ctx.body = "Monitoring like never before. [TODO] add prometheus endpoint possibly?"

  await next()
})

module.exports = router.routes()
