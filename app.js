const Koa = require("koa")
const router = require("koa-router")()
const applicationsEndpoint = require("./src/applications/endpoint")
const rootEndpoint = require("./src/root/endpoint")
const db = require("./src/modules/db")

const app = new Koa()
const _port = process.env.PORT || 1234

router.use("/", rootEndpoint)
router.use("/applications", applicationsEndpoint)
app.use(router.routes())

const bootUp = async () => {
  const connectionString = process.env.MONGODB

  try {
    await db.startup(connectionString)

    app.listen(_port)
    console.log("Listening on port", _port)
  } catch (err) {
    console.error("Error booting application", err)
  }
}

const terminate = (signal) => {
  console.log(`Application stopping on ${signal}`)

  db.close()
  process.exit()
}

process.on('SIGINT', () => {
  terminate('SIGINT')
})

process.on('SIGTERM', () => {
  terminate('SIGTERM')
})

bootUp()
